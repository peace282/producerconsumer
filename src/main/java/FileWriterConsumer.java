import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class FileWriterConsumer implements Runnable {
    private BlockingQueue<String> outputQueue = new LinkedBlockingQueue<>();
    private String filePath;
    public FileWriterConsumer(BlockingQueue<String> outputQueue, String filePath) {
        this.outputQueue = outputQueue;
        this.filePath = filePath;
    }

    @Override
    public void run() {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(this.filePath))) {
            while (true) {
                String result = outputQueue.take();
                if (result.equals("&_&")) {
                    break;
                }
                writer.write(result);
                writer.newLine();
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
