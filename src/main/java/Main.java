import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Main {
    static BlockingQueue<String> inputQueue = new LinkedBlockingQueue<>();
    static BlockingQueue<String> outputQueue = new LinkedBlockingQueue<>();

    public static void main(String[] args) {

        int consumerNumber = 3;
        List<Thread> consumerList = new ArrayList<>();
        Thread thread = new Thread();
        String inputPath = "src/main/java/input.txt";
        String outputPath = "output.txt";

        Producer producer = new Producer(inputQueue, consumerNumber, inputPath);
        Thread producerThread = new Thread(producer);

        Consumer consumer = new Consumer(inputQueue,outputQueue);
        for (int i = 0; i<consumerNumber; i++) {
            thread = new Thread(consumer);
            consumerList.add(thread);
        }

        FileWriterConsumer fileWriter = new FileWriterConsumer(outputQueue, outputPath);
        Thread writerThread = new Thread(fileWriter);

        producerThread.start();
        for (Thread th : consumerList) {
            th.start();
        }
        writerThread.start();
    }
}
