import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
public class Consumer implements Runnable{
    private BlockingQueue<String> inputQueue = new LinkedBlockingQueue<>();
    private BlockingQueue<String> outputQueue = new LinkedBlockingQueue<>();

    public Consumer(BlockingQueue<String> inputQueue, BlockingQueue<String> outputQueue) {
        this.inputQueue = inputQueue;

        this.outputQueue = outputQueue;
    }

    @Override
    public void run() {
        try {
            while (true) {
                String line = inputQueue.take();
                // Check for the "poison pill" value
                if (line.equals("$_$")) {
                    if(inputQueue.size() == 0){
                        outputQueue.put("&_&");
                    }
                    // Stop the consumer thread
                    break;
                }
                String processedLine = processLine(line);
                outputQueue.put(processedLine);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static String processLine(String line) {
        return line.toUpperCase();
    }

//    private static String processLine(String line) {
//        String[] words = line.split(" ");
////
//        StringBuilder stemmedLine = new StringBuilder();
//        for (String word : words) {
//            String stemmedWord = stemWord(word);
//            stemmedLine.append(stemmedWord).append(" ");
//        }
//
//        return stemmedLine.toString().trim();
//    }
//
//    private static String stemWord(String word) {
////        // For example, a simple algorithm could be to remove common suffixes like "", "", "ly", etc.
//        if (word.endsWith("ing")) {
//            return word.substring(0, word.length() - 3); // Remove "ing" suffix
//        } else if (word.endsWith("ed") || (word.endsWith("ly"))) {
//            return word.substring(0, word.length() - 2); // Remove "ed" suffix
//        }
//        return word;
//    }
}
