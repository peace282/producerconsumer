import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Producer implements Runnable{
    private BlockingQueue<String> inputQueue = new LinkedBlockingQueue<>();
    private String filePath;
    private int consumersNumber;

    public Producer(BlockingQueue<String> inputQueue, int consumersNumber, String filePath){
        this.inputQueue = inputQueue;
        this.consumersNumber = consumersNumber;
        this.filePath = filePath;
    }

    @Override
    public void run() {
        try (BufferedReader reader = new BufferedReader(new FileReader(this.filePath))) {
            String line;
            while ((line = reader.readLine()) != null) {
                inputQueue.put(line);
            }
            for (int k = 0; k < consumersNumber; k++)
                inputQueue.put("$_$");
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
