import java.io.*;
import java.util.concurrent.*;

public class FileProcessor {
    static BlockingQueue<String> inputQueue = new LinkedBlockingQueue<>();
    public static void main(String[] args) {

        BlockingQueue<String> outputQueue = new LinkedBlockingQueue<>();

        // Producer
        Thread producer = new Thread(() -> {
            try (BufferedReader reader = new BufferedReader(new FileReader("C:\\Users\\Baylasan\\IdeaProjects\\producer_consumer\\src\\main\\java\\input.txt"))) {
                String line;
//                int i=0;
                while ((line = reader.readLine()) != null) {
                    inputQueue.put(line);
//                    i++;
//                    System.out.println("\nFile Reader Thrid" + line + i);
                }
//                System.out.println("\nInput Queue Size" + inputQueue.size() );
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        });
        System.out.println("\nInput Queue Size" + inputQueue.size() );
        // Consumers
        int numConsumers = 3;
        Thread[] consumers = new Thread[numConsumers];
        for (int i = 0; i < numConsumers; i++) {
//            System.out.println("\nInput Queue Size" + inputQueue.size() );
            consumers[i] = new Thread(() -> {
                try {
                    int k = 0;
//                    System.out.println("\nInput Queue Size" + inputQueue.size() );
                    while (inputQueue.size()>0) {
                        String line = inputQueue.take();
                        String processedLine = processLine(line); // Replace with your processing logic
                        outputQueue.put(processedLine);
                        k++;
//                        System.out.println("\nFile Process line Thrid" + line + k);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }

        // Consumer for writing to file
        Thread fileWriter = new Thread(() -> {
            try (BufferedWriter writer = new BufferedWriter(new FileWriter("output.txt"))) {
                while (outputQueue.size()>0) {
                    String result = outputQueue.take();
                    writer.write(result);
                    writer.newLine();
                }
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        });

        // Start all threads
        producer.start();
        for (Thread consumer : consumers) {
            consumer.start();
        }
        fileWriter.start();

        // Wait for all threads to finish
        try {
            producer.join();
            for (Thread consumer : consumers) {
                consumer.join();
            }
            fileWriter.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static String processLine(String line) {
        // Replace this with your actual processing logic
        return line.toUpperCase(); // Example: convert line to uppercase
    }
}
